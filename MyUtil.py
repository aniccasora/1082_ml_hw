import random
import cv2
import numpy as np

# 生成隨機色碼List
def get_rand_colorList(numberOfcolor: int):
    return ["#" + "".join([hex(random.randint(0, 15))[-1] for i in range(6)]) for j in range(numberOfcolor)]

# 將圖片(img)解析度降至 指定的值(res) 以下
def get_reduce_resolution_img(res:int, img:np.ndarray):
    if res < 0:
        return img
    # 圖片寬、長
    w, h = img.shape[1::-1]

    # 縮放比率，會越來越高
    ratio = 1

    # 尋找合適的 ratio
    while ( int(w/ratio)*int(h/ratio) > res ):
        ratio += 1

    # 得到了 合適的 ratio
    print("--- Reduced (w, h) = ({}, {})".format(int(w/ratio),int(h/ratio)))

    # INTER_AREA 此重新採樣 可以去除摩爾紋的問題。
    return cv2.resize(img.copy(),
             dsize=(int(w/ratio), int(h/ratio)),
             interpolation=cv2.INTER_AREA)

