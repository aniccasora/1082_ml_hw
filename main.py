import numpy as np
import cv2
import matplotlib.pyplot as plt
# Figure, FigureCanvas 幫助 pyplot 的 figure 轉成 nparray。
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from sklearn.cluster import KMeans
from MyUtil import get_reduce_resolution_img
import glob


if __name__ == "__main__":

    # K 從 2++ 重複 K_time 次
    K_time = 20

    # "最大總像素" 參數，<0 時用原解析度做計算。(縮小原圖用)
    RES_PARAM = -1

    # 獲取output 資料夾下的所有檔案名稱
    for fname in glob.glob(".\img\*.*"):
        img = cv2.imread(fname, cv2.IMREAD_COLOR)
        # 原始的 彩色圖片副本。
        img_o = np.copy(img)
        # 灰階圖，用來評估圖片平均亮度的。
        img_grey = cv2.cvtColor(np.copy(img_o), cv2.COLOR_BGR2GRAY)
        # 圖片平均亮度，至少要 > 180。
        img_mean_lightness = 180 if int(np.mean(img_grey)) < 180 else 180
        print("Image mean lightness: ", img_mean_lightness)
        # 取得單獨檔名(不管副檔名)
        img_name = fname.split('\\')[-1].split('.')[0]

        # k-means 的 k。
        K_list = [i+2 for i in range(K_time)]

        for K in K_list:

            # 將輸入的圖片轉成 YUV 色彩空間，原因是因為我要捨去Y(亮度)，只考慮U、V(色彩)，這樣 function 也好 call >v<。
            img = cv2.cvtColor(np.copy(img_o), cv2.COLOR_BGR2YUV)

            # YUV 的圖片 以視覺來說 淺粉、淺綠為主，較淡色。
            # cv2.imshow("YUV",img)
            # cv2.waitKey(0)

            # 測試期讓 k-means 不要跑太久用的，RES_PARAM < 0 時，
            # 就會用原本的解析度下去計算，否則會將該圖片降至 指定解析度以下。
            img = get_reduce_resolution_img(RES_PARAM, img)
            img_o =  get_reduce_resolution_img(RES_PARAM, img_o)

            print("Reduce ver. image shape (h, w, channel) : {}".format(img.shape))
            w, h = img.shape[1::-1]
            print("Origin size: (w:{}, h:{})".format(w,h))
            # 實際處理解析度(圖片有縮放時會變動)
            img_pixel= w * h

            # LUV-space 的 image 只提取， U、V 空間的座標。
            # U 值
            img_u = img[:, :, 1].flatten()
            # V 值
            img_v = img[:, :, 2].flatten()

            # 提取 L通道 平均數，之後用來評估視覺化的顏色資訊。(使用中位在這邊是不必要的，因輸出基本相同)
            img_mean = np.mean(img[:, :, 0].flatten())
            print("img_mean: ", img_mean)

            # (U, V) 座標組 的 list宣告。
            img_uv = []

            # 顯示進度用
            progress = 0
            total = w * h
            # 開始塞 (Ux, Vx)....x=0~(w*h)
            for item in zip(img_u,img_v):
                progress += 1
                print("Appending... {:4.2f} %".format((progress / total) * 100), end="")
                img_uv.append(item)
                print("\r",end="")
            print("Append complete !")

            print("asarraying ...", end="")
            # [[u0 v0],[u1 v1],...], list cvt2 ndarray (size is w*h, (by input img))
            img_uv = np.asarray(img_uv)
            print("\rasarray complete!")
            # 屌 fit 一波

            print("fitting ...", end="")
            kmeans = KMeans(n_clusters=K,init='k-means++',
                            precompute_distances='auto',
                            random_state=None).fit(img_uv)
            print("\rfitting complete!")

            # 取得 所有點的 labels (總共有 w * h 個)
            label_pred = kmeans.labels_
            numbers_of_label = len(kmeans.labels_)
            # 看 labels 用。 EX: [0 0 0 ... 1 1 1] 之類的... (總共有 w * h 個)
            # print("kmeans Labels: {}".format(label_pred))

            # 將不同類的點 分別記錄下來 各項(img_uv) 會是
            # [[u0, v0], [u1, v1], ..., [ux, vx]] 座標組 (LUV 色彩空間的座標，但只保留 UV 資訊)。
            # 因為 img_uv、label_pred 的大小一樣，位置也是對應的。
            # =  =  =  =  =  =  =  =  =  =
            # label_pred 要是 ndarray, 才能用這種功能(numpy mask)
            # label_pred [0 0 1 1 0 0]
            # [label_pred == 0 ] -> [T T F F T T] (mask)

            # 生成 N * K 的二維list
            type_K = [[] for i in range(K)]

            for type_number in range(K):
                type_K[type_number] = img_uv[label_pred == type_number]

            # 取得 中心點。
            centers = np.array(kmeans.cluster_centers_)
            # print("centers: ", centers)

            # means center color, 先製造出 color list。
            center_color_YUV = []
            for i in range(K):
                center_color_YUV.append((img_mean, centers[i][0], centers[i][1]))

            # K類的 中心點RGB 數值。
            center_color_RGB = []

            # YUV to RGB
            for item in center_color_YUV:
                tmp_U, tmp_V = item[1], item[2]
                tmp_Y = img_mean_lightness
                tmp_R = int(tmp_Y + 1.13983 * (tmp_V - 128))
                tmp_G = int(tmp_Y - 0.39465 * (tmp_U - 128) - 0.5806 * (tmp_V - 128))
                tmp_B = int(tmp_Y + 2.03211 * (tmp_U - 128))
                # print((tmp_R,tmp_G,tmp_B))
                # 因上方公式有浮點數，計算出來極可能會有 -0.5, 255.5...的問題，故作下方處理。
                tmp_R = 0   if tmp_R < 0    else tmp_R
                tmp_R = 255 if tmp_R > 255  else tmp_R
                tmp_G = 0   if tmp_G < 0    else tmp_G
                tmp_G = 255 if tmp_G > 255  else tmp_G
                tmp_B = 0   if tmp_B < 0    else tmp_B
                tmp_B = 255 if tmp_B > 255  else tmp_B
                center_color_RGB.append((tmp_R, tmp_G, tmp_B))

            # plt.scatter 時用的顏色 list, ex: "#AABBCC"...
            # "1"(要滿二位數才行) -- zfill(2) --> "01" (YA)
            type_color_list = \
            [("#" + hex(center_color_RGB[typeN][0])[2:].zfill(2) +
                    hex(center_color_RGB[typeN][1])[2:].zfill(2) +
                    hex(center_color_RGB[typeN][2])[2:].zfill(2)) for typeN in range(K)]

            #
            fig = Figure()
            canvas = FigureCanvas(fig)

            fig.tight_layout(pad=0)
            ax = fig.add_subplot(111)
            # 調整周圍的白邊
            fig.subplots_adjust(top=0.99, bottom=0.1, right=0.99, left=0.1,
                                hspace=0.2, wspace=0.2)
            ax.set_xlabel("u")
            ax.set_ylabel("v")
            #

            # 用 matplotlib 繪製 散點圖(scatter)
            for typeN in range(K) :
                ax.scatter(type_K[typeN][:, 0], type_K[typeN][:, 1], marker="o", color=type_color_list[typeN])

            # 再另外 繪製中心點
            ax.scatter(centers[:, 0], centers[:, 1], marker="x", color='r', )

            # print("centers: {}".format(centers))
            print("centers.shape: {}".format(centers.shape))

            # 對應用 空白畫布(他是三通道的，用來產生視覺效果用的)
            ddddd = np.zeros(shape=(img.shape), dtype=np.uint8, order='F')
            # 他由這下列這幾個 作為 R、G、B 單通道。 等等下面會設定顏色數值。
            ddddd_R = ddddd[:, :, 0].flatten()
            ddddd_G = ddddd[:, :, 1].flatten()
            ddddd_B = ddddd[:, :, 2].flatten()

            progress = 0
            total = w * h

            for idx in range(total):
                progress += 1
                print("scatting... {:4.2f} %".format((progress / total) * 100), end="")

                pred_type = label_pred[idx]

                # 不同的type 之間給 "不同顏色"。
                ddddd_R[idx] = center_color_RGB[pred_type][2]
                ddddd_G[idx] = center_color_RGB[pred_type][1]
                ddddd_B[idx] = center_color_RGB[pred_type][0]
                print("\r", end="")
            print("scatting... 100.0 %")

            # 將生產出的 三通道 合成一張圖片，利於人眼觀察分類結果。
            rgb = np.dstack((ddddd_R, ddddd_G, ddddd_B)).reshape(img.shape)
            output = np.hstack((img_o, rgb))
            # cv2.imshow(str(K)+"_means_rgb", output)
            # cv2.imwrite("./output/"+str(K)+"_means_rgb.jpg", output)

            fig.canvas.draw()
            # 將 fig 轉成 numpy.array
            image_from_plot = np.frombuffer(fig.canvas.tostring_rgb(), dtype=np.uint8)
            image_from_plot = image_from_plot.reshape(fig.canvas.get_width_height()[::-1] + (3,))
            image_from_plot = cv2.cvtColor(image_from_plot, cv2.COLOR_RGB2BGR)
            sca_img_w, sca_img_h =  image_from_plot.shape[1::-1]
            # 本來應是 plt.show() 的
            fin_output = np.vstack((output,
                                   cv2.resize(image_from_plot, (2 * w, int(2 * w * sca_img_h / sca_img_w))))
                                  )
            # cv2.imshow("fig_image",fn_output)
            fin_name = img_name +"_"+ str(K) + "_mean"
            cv2.imwrite("./output/"+ fin_name +".jpg",fin_output)
            # cv2.waitKey(0)
